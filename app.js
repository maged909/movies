require('dotenv').config()

const express = require('express');
const path = require('path');
const logger = require('morgan');
const bodyParser = require('body-parser');
const neo4j = require('neo4j-driver');
const { env } = require('process');

const app = express();

app.set('views',path.join(__dirname,'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extends:false}))
app.use(express.static(path.join(__dirname,'public')))

const driver = neo4j.driver('bolt://localhost:7687',neo4j.auth.basic(process.env.NEO_USERNAME,process.env.NEO_PASSWORD))
const session = driver.session();

app.get('/',function(req,res){
    var page = parseInt(req.query.page) || 1
    var Default = parseInt(req.query.limit) || 10
    var limit = parseInt(req.query.limit) +1 || 11
    var q = req.query.q || ''

    session
        .run("MATCH(n:Movie) WHERE tolower(n.title) CONTAINS tolower($q) RETURN n ORDER BY n.createdAt DESC SKIP $skip LIMIT $limit",{limit:neo4j.int(limit),skip:neo4j.int((page-1)*limit),q:q})
        .then(function(results){
            if(results.records.length <= Default ){
                var nextPage = false
            }else{
                var nextPage = true
                results.records.pop();}
            if(page > 1){
                var prevPage = true
            }else{var prevPage = false}

            var movies=[];
            results.records.forEach(function(record){
                movies.push({
                    id:record._fields[0].identity.low,
                    title: record._fields[0].properties.title,
                    released: record._fields[0].properties.released,
                    tagline: record._fields[0].properties.tagline,
                    
                });
            });
            res.render('index.ejs',{pagination:{page:page,limit:limit,prevPage:prevPage,nextPage:nextPage},search:q,movies:movies});
        })
        .catch((err)=>{
            console.log(err)
        });
})
app.get('/persons',function(req,res){
            var page = parseInt(req.query.page) || 1
            var Default = parseInt(req.query.limit) || 10
            var limit = parseInt(req.query.limit) +1 || 11
            var q = req.query.q || ''
            session
                .run("MATCH(n:Person) WHERE tolower(n.name) CONTAINS tolower($q) RETURN n SKIP $skip LIMIT $limit",{limit:neo4j.int(limit),skip:neo4j.int((page-1)*limit),q:q})
                .then(function(results2){
                    if(results2.records.length <= Default ){
                        var nextPage = false
                    }else{
                        var nextPage = true
                        results2.records.pop();}
                    if(page > 1){
                        var prevPage = true
                    }else{var prevPage = false}
                    var persons=[];
                    results2.records.forEach(function(record){
                    persons.push({
                        id:record._fields[0].identity.low,
                        name: record._fields[0].properties.name,
                        born: record._fields[0].properties.born,
                    });
                });
                res.render('persons.ejs',{pagination:{page:page,limit:limit,prevPage:prevPage,nextPage:nextPage},search:q,persons:persons});
                })
                .catch((err)=>{
                    console.log(err)
                });
})
app.get('/movies/addForm',(req,res)=>{
    res.render('movieForm.ejs');
})
app.post('/movies/add',(req,res)=>{
    var title=req.body.title
    var year = req.body.year
    var date = new Date()
    var now = (date.getTime() + date.toLocaleString()).toString()
    // console.log(now);
    session
        .run('CREATE(n:Movie {title:$titleParams,released:$yearParams,createdAt:$now}) RETURN n',{titleParams:title,yearParams:year,now:now})
        .then(function(){
            res.redirect('/')

            
        })
        .catch((err)=>{
            console.log(err)
        })
})

app.listen(7687)
console.log('server\'s listening port 7687')

module.exports = app;
